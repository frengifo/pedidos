@extends('layout')
@section('content')

	<section id="cart_items">
		<div class="container">
			<div class="register-req">
				<p>Porfavor complete todos los campos............</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-12 clearfix">
						<div class="bill-to">
							<p>Detalles de Entrega</p>
							<div class="form-one">
								<form action="{{url('/save-shipping-details')}}" method="post">
									{{csrf_field()}}
								<input type="text" name="shipping_email"  placeholder="Correo Electronico*" required="">
								<input type="text" name="shipping_first_name"  placeholder="Nombres *" required="">
								<input type="text"  name="shipping_last_name" placeholder="Apellidos *" required="">
								<input type="text" name="shipping_address"  placeholder="Direccion *">
								<input type="text" name="shipping_mobile_number"  placeholder="Telefono *" required="">
                                <input type="text" name="shipping_city"  placeholder="Referencia *" required=""> 
                                <input type="submit" class="btn btn-primary" value="Listo!" required="">
								</form>
							</div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->



@endsection
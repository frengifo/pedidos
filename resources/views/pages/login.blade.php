@extends('layout')
@section('content')
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Iniciar Sesión</h2>
						<form action="{{url('/customer_login')}}" method="post">
						    {{csrf_field()}}
							<input type="email" required="" placeholder="Correo electronico" name="customer_email" />
							<input type="password" placeholder="Contraseña" name="password" />
							<button type="submit" class="btn btn-default">Ingresar</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">O</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Registrate y realiza tu Pedido!</h2>
						<form action="{{url('/customer_registration')}}" method="post">
							{{csrf_field()}}
							<input type="text" placeholder="Nombres Completos" name="customer_name" required="" />
							<input type="email" placeholder="Correo electronico" name="customer_email" required=""/>
							<input type="password" placeholder="Contraseña" name="password" required=""/>
							<input type="text" placeholder="Telefono" name="mobile_number" required=""/>
							<button type="submit" class="btn btn-default">Registrarme</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	


@endsection